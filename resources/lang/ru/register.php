<?php

return [
    'login' => 'Вход',
    'register' => 'Зарегистрироваться',
    'name' => 'Имя',
    'email' => 'E-Mail Адресс',
    'password' => 'Пароль',
    'confirm' => 'Подтвердите пароль',
];
