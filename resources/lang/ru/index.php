<?php

return [
    'phrases' => 'Фразы для перевода',
    'phrase_table' => 'Фраза',
    'no_phrase' => 'Нет фраз для перевода',
    'add_phrase' => 'Добавить фразу',
    'enter' => 'Введите фразу на русском языке',
    'enter_button' => 'Ввести'
];
