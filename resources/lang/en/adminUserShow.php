<?php

return [
    'create' => 'Create new photo',
    'profile' => 'Profile of',
    'remove' => 'Remove',
];
