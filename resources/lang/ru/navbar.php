<?php

return [
    'main' => 'Фото Галерея',
    'login' => 'Вход',
    'register' => 'Регистрация',
    'logout' => 'Выйти',
    'myprofile' => 'Мой профиль',
    'hello' => 'Приветствую'
];
