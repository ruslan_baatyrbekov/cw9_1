<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class dashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $photos = Photo::all();
        $users = User::all();
        return view('superAdmin.dashboard.index',compact('photos','users'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        $photos = Photo::all();
        return view('superAdmin.dashboard.show',compact('photos','user'));
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function makeAdmin(User $user){
        $user->admin = true;
        $user->update();
        return redirect()->back();
    }

    /**
     * @param Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyPhoto(Photo $photo)
    {
        $photo->delete();
        return redirect()->back();
    }
}
