<?php

return [
    'verify' => 'Verify Your Email Address',
    'fresh' => 'A fresh verification link has been sent to your email address.',
    'before' => 'Before proceeding, please check your email for a verification link.',
    'if' => 'If you did not receive the email',
    'click' => 'click here to request another',
];
