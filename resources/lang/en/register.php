<?php

return [
    'login' => 'Login',
    'register' => 'Register',
    'name' => 'Name',
    'email' => 'E-Mail Address',
    'password' => 'Password',
    'confirm' => 'Confirm Password',
];
