@extends('layouts.app')

@section('content')
            <div class="card p-2 m-1 mb-4 shadow-lg">
                <img src="{{asset('/storage/' . $photo->image)}}" class="card-img-top" alt="...">
                <div class="card-body row">
                    <div class="col">
                        <p class="card-text">{{$photo->name}}</p>
                        <p class="card-footer"><a href="{{route('admin.user.show', ['user' => $photo->user])}}">{{$photo->user->name}}</a></p>
                        <p>{{$photo->created_at}}</p>
                        <p>@lang('userPhotoShow.score'):{{$avarage}}</p>
                    </div>
                </div>
            </div>
            <div class="card p-2 m-1 mb-4 shadow-lg">
                <div>
                    <h3>@lang('userPhotoShow.comments')</h3>
                    @foreach($comments as $comment)
                        @if($comment->photo->id == $photo->id)
                            <div class="card">
                                <p class="card-body">{{$comment->user->name}}:</bold>"{{$comment->body}}" </p>
                                <p class="card-footer text-small">rating: {{$comment->rating}}</p>
                            </div>
                        @endif
                    @endforeach
                </div>
                @if(\Illuminate\Support\Facades\Auth::user())
                    <form method="post" action="{{route('admin.comment.store',['photo' => $photo])}}">
                        @csrf
                        <div class="form-group shadow-sm">
                            <label for="rating" class="mt-1">@lang('userPhotoShow.rating'):</label>
                            <select class="form-control" id="rating" name="rating">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                            @error('rating')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                            <label for="Comment" class="mt-1">@lang('userPhotoShow.comment'):</label>
                            <textarea class="form-control" id="body" rows="3" name="body"></textarea>
                            @error('body')
                            <div class="alert alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-secondary">@lang('userPhotoShow.add')</button>
                    </form>
            </div>
            @endif
@endsection
