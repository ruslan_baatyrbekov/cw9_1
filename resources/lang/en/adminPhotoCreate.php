<?php

return [
    'add' => 'Add new photo',
    'name' => 'Name',
    'image' => 'Image',
    'addimage' => 'Add image',
    'save' => 'Save',
];
