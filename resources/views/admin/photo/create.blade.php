@extends('layouts.app')

@section('content')
    <div class="shadow-lg">
        <h1>@lang('adminPhotoCreate.add')</h1>
        <form enctype="multipart/form-data" method="post" action="{{route('admin.photo.store')}}">
            @csrf
            <div class="form-group">
                <label for="name" class="mt-1">@lang('adminPhotoCreate.name'):</label>
                <input type="text" class="form-control" id="name" name="name">
                @error('name')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
                <label for="image" class="mt-1">@lang('adminPhotoCreate.image'):</label>
                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile" name="image">
                        <label class="custom-file-label" for="customFile">@lang('adminPhotoCreate.addimage')</label>
                    </div>
                </div>
                @error('picture')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-secondary">@lang('adminPhotoCreate.save')</button>
        </form>
    </div>
@endsection
