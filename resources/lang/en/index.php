<?php

return [
    'phrases' => 'Phrases for translation',
    'phrase_table' => 'Phrase',
    'no_phrase' => 'No phrases to translate',
    'add_phrase' => 'Add phrase',
    'enter' => 'Enter the phrase in Russian',
    'enter_button' => 'Enter'
];
