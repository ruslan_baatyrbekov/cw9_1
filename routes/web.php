<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function(){
    Route::get('/', [\App\Http\Controllers\user\PhotoController::class, 'index'])->name('/');
    Route::get('photo/{photo}',[\App\Http\Controllers\user\PhotoController::class,'show'])->name('user.photo.show');
    Route::get('{user}/show',[\App\Http\Controllers\admin\UserController::class,'show'])->name('admin.user.show')->middleware('auth');
    Route::get('{user}/create',[\App\Http\Controllers\admin\UserController::class,'create'])->name('admin.photo.create')->middleware('auth');
    Route::post('photo/create',[\App\Http\Controllers\admin\UserController::class,'store'])->name('admin.photo.store')->middleware('auth');
    Route::delete('admin/{photo}/destroy',[\App\Http\Controllers\admin\UserController::class,'destroy'])->name('admin.photo.destroy')->middleware('auth');
    Route::get('myprofile',[\App\Http\Controllers\admin\UserController::class,'MyProfile'])->name('admin.myprofile');
    Route::post('comment/{photo}/store',[\App\Http\Controllers\admin\CommentController::class,'store'])->name('admin.comment.store')->middleware('auth');

    Route::get('dashboard',[\App\Http\Controllers\dashboard\DashboardController::class,'index'])->name('dashboard');
    Route::get('dashboard/{user}',[\App\Http\Controllers\dashboard\DashboardController::class,'show'])->name('dashboard.show');
    Route::post('dashboard/admin/{user}',[\App\Http\Controllers\dashboard\DashboardController::class,'makeAdmin'])->name('dashboard.makeadmin');
    Route::delete('dashboard/{photo}',[\App\Http\Controllers\dashboard\DashboardController::class,'destroyPhoto'])->name('dashboard.destroyPhoto');

    Auth::routes();
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Route::get('language/{locale}', [\App\Http\Controllers\LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')
    ->where('locale', 'ru|en');
