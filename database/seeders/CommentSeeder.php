<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $photos = Photo::all();
        for($i = 0; $i < 10; $i++) {
            Comment::factory()
                ->for($users->random())
                ->for($photos->random())
                ->create();
        }
    }
}
