<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request,Photo $photo)
    {
        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->photo_id = $photo->id;
        $comment->body = $request->input('body');
        $comment->rating = $request->input('rating');
        $comment->save();
        return redirect()->back()->with('status','comment was added');
    }
}
