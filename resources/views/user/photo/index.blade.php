@extends('layouts.app')

@section('content')
    <div class="row row-cols-lg-4 row-cols-md-4 row-cols-sm-2 shadow-lg">
     @foreach($photos as $photo)
             <div class="card p-2">
                 <img src="{{asset('/storage/' . $photo->image)}}" class="card-img-top" alt="...">
                 <div class="card-body row">
                     <div class="col">
                         <p class="card-text"><a href="{{route('user.photo.show', ['photo' => $photo])}}">{{$photo->name}}</a></p>
                         <p class="card-footer"><a href="{{route('admin.user.show', ['user' => $photo->user])}}">{{$photo->user->name}}</a></p>
                     </div>
                 </div>
             </div>
        @endforeach
    </div>
@endsection
