<?php

return [
    'main' => 'Photo Gallery',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'LogOut',
    'myprofile' => 'My Profile',
    'hello' => 'Hello'
];
