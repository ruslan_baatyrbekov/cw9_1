<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\admin\UserRequest;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $photos = Photo::all();
        $auth_user = Auth::user();
        return view('admin.user.show',compact('photos','user','auth_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.photo.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $photo = new Photo();
        $photo->name = $request->input('name');
        if($request->hasFile('image')){
            $photo->image = $request->file('image')->store('photos','public');
        }
        $photo->user_id = Auth::user()->id;
        $photo->save();
        return redirect()->back()->with('status','Photo was create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo $photo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();
        return redirect()->back();
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function MyProfile(User $user)
    {
        $photos = Photo::all();
        $auth_user = Auth::user();
        return view('admin.user.show',['user' => $auth_user, 'auth_user' => $auth_user,'photos' => $photos]);
    }

}
