<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Photo;
use http\Client\Curl\User;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::all()->sortByDesc('created_at');
        return view('user.photo.index',compact('photos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        $comments = Comment::all();
        $avarageScoreArray = [];
        foreach ($comments as $comment){
            if($comment->photo_id == $photo->id){
                $avarageScoreArray[] = $comment->rating;
            }
        }
        $avarage = round(array_sum($avarageScoreArray)/count($avarageScoreArray),1);
        return view('user.photo.show',compact('photo','comments','avarage'));
    }
}
