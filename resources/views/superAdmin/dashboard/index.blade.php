@extends('layouts.app')

@section('content')
    <div class="shadow-lg">
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Статус</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td><a href="{{route('dashboard.show',['user' => $user])}}">{{$user->name}}</a></td>
                <td>
                    @if($user->admin == null)
                        <form action="{{route('dashboard.makeadmin',['user' => $user])}}" method="post">
                            @method('POST')
                            @csrf
                            <button class="btn btn-sm btn-danger btn-lg dashboard-bt" type="submit">назначить администратором</button>
                        </form>
                    @else
                        Админ
                    @endif

                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
