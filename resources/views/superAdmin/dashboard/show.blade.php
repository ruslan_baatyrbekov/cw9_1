@extends('layouts.app')

@section('content')
    <div class="shadow-lg">
        <h1>Фото пользователя</h1>
        <table class="table table-dark">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Фото</th>
                <th scope="col">Название</th>
                <th scope="col">Действие</th>
            </tr>
            </thead>
            <tbody>
            @foreach($photos as $photo)
                @if($photo->user_id == $user->id)
                <tr>
                    <th scope="row">{{$photo->id}}</th>
                    <td><img src="{{asset('/storage/' . $photo->image)}}" class="card-img-top" alt="..." style="height: 5rem; width:5rem; "></td>
                    <td>{{$photo->name}}</td>
                    <td>
                        <form action="{{route('dashboard.destroyPhoto',['photo' => $photo])}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-sm btn-danger btn-lg dashboard-bt" type="submit">Удалить</button>
                        </form>
                    </td>
                </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
