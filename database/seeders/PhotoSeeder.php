<?php

namespace Database\Seeders;

use App\Models\Photo;
use App\Models\User;
use Database\Factories\PhotoFactory;
use Illuminate\Database\Seeder;

class PhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5; $i++) {
            Photo::factory()->count(rand(1, 3))->for(User::factory())->create();
        }
    }
}
