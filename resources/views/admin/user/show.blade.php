@extends('layouts.app')

@section('content')
    <h1>@lang('adminUserShow.profile') {{$user->name}}</h1>
    @if($user == $auth_user)
        <a href="{{route('admin.photo.create', ['user' => $user])}}">@lang('adminUserShow.create')</a>
    @endif
    <div class="row row-cols-lg-4 row-cols-md-4 row-cols-sm-2 shadow-lg">
        @foreach($photos as $photo)
            @if($photo->user_id == $user->id)
            <div class="card p-2 m-1 ">
                <img src="{{asset('/storage/' . $photo->image)}}" class="card-img-top" alt="...">
                <div class="card-body row">
                    <div class="col">
                        <p class="card-text"><a href="{{route('user.photo.show', ['photo' => $photo])}}">{{$photo->name}}</a></p>
                        <p class="card-footer">{{$photo->user->name}}</p>
                        @if($user == $auth_user)
                            <form action="{{route('admin.photo.destroy',['photo' => $photo])}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-sm btn-danger btn-lg dashboard-bt" type="submit">@lang('adminUserShow.remove')</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        @endforeach
    </div>
@endsection
