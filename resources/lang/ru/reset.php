<?php

return [
    'reset' => 'Сброс пароля',
    'email' => 'E-Mail Адресс',
    'confirm' => 'Подтвердите пароль',
    'password' => 'Пароль',
];
